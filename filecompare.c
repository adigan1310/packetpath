/******************************************************************************
						PacketPath LLC Assessment
Program to compare two binary files and print the time taken to compare the files.
The function also prints the offset at which the difference starts and the next 
16 bytes from that difference position.

Written by - Adithya Ganapathy 07/24/2018

*******************************************************************************/

#include <stdio.h>
#include <time.h>


void comparefiles (FILE * fp1, FILE * fp2)
{
  char ch1, ch2;
  int fl = 1,i;
  int op1[16],op2[16];
  clock_t start,end;
  
  start = clock();
  while(!feof(fp1))   
  {
      if (fgetc(fp1) != fgetc(fp2))
	  {
	    fl = 0;
	    break;
	  }
  }
  end = clock();
  
  fseek (fp1, -1, SEEK_CUR);
  fseek (fp2, -1, SEEK_CUR);
  if (fl == 1)
  {
     printf ("Two files are equal");
  }
  else
  {
     printf ("Two files are not equal\n");
     printf ("Byte position at which they differ : %ld \n", ftell (fp1) + 1);
     
     printf("First File:\n");
     fread (op1, ftell (fp1) + 1, 16, fp1);
     for (i = 0; i < 16; i++)
	 {
	   printf ("%c ", op1[i]);
	 }
	 
	 printf("\nSecond File:\n");
	 fread (op2, ftell (fp2) + 1, 16, fp2);
     for (i = 0; i < 16; i++)
	 {
	   printf ("%c ", op2[i]);
	 }
  }
  printf("\nTime taken to compare files : %f s",((float)(end - start)/ CLOCKS_PER_SEC));
}

int main (int argc, char *argv[])
{
    FILE *fp1, *fp2;
  
    if (argc < 3)
    {
      printf ("Please provide two filenames.");
      return 0;
    }
    else
    {
      fp1 = fopen (argv[1], "rb");
      fp2 = fopen (argv[2], "rb");
        
      if (fp1 == NULL)
	  {
	    printf ("Unable to open first file");
	    return 0;
	  }

      if (fp2 == NULL)
	  {
	    printf ("Unable to open second file");
	    return 0;
	  }

      comparefiles (fp1, fp2);
    }
	return 0;
}